import React from 'react'
import { FaTrashAlt } from 'react-icons/fa';

function LineItem({ item, handleCkeck, handleDelete }) {
    return (

        <li className="item">
            {/* use by anonymous function with the fuinction Expression with handleCkeck and the argument of key method of list  */}
            <input type="checkbox" onChange={() => handleCkeck(item.id)} checked={item.checked} />
            <label
                style={(item.checked) ? { textDecoration: 'line-through' } : null}
                onDoubleClick={() => handleCkeck(item.id)}>
                {item.item}
            </label>
            < FaTrashAlt
                onClick={() => handleDelete(item.id)}
                role='button' tabIndex="0"
                arial-label={`Delete ${item.item}`}
            />

        </li>



    )
}

export default LineItem