import React from 'react'

function Footer({ length }) {
    // Create the logic about the date Today
    // const today = new Date();
    return (
        <footer>
            <p>{length} List {length === 1 ? "item" : "items"}</p>
        </footer>
    )
}

export default Footer