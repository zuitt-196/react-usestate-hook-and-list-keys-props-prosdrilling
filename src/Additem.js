import React from 'react'
import { FaPlus } from 'react-icons/fa';
import { useRef } from 'react'

const Additem = ({ newItem, setINewtems, handleSubmit }) => {
    // Define and initilize the inputRef with value of useRef Hooks
    //which to make fucos the element it is main to change or mutable the the current propperty with initillized the pass argumnet 
    // or the other explanation the useRef it is all about in  how to mutable or change the current proppert or argument ,method etc.
    // that is use of useRef comon use for animation and button 
    const inputRef = useRef();

    return (
        <form className="addForm" onSubmit={handleSubmit}>
            <label htmlFor="addItem">Add Item</label>
            <input autoFocus
                ref={inputRef}
                id="addItem" type="text"
                placeholder='Add Item'
                required
                value={newItem}
                onChange={(e) => setINewtems(e.target.value)}
            />
            <button
                type="submit"
                aria-label='Add Item'

                onClick={() => inputRef.current.focus()}
            >
                <FaPlus />
            </button>
        </form>
    )
}

export default Additem