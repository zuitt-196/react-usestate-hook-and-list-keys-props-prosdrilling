import React from 'react'

import ItemList from './ItemList'


const Content = ({ items, handleCkeck, handleDelete }) => {


    return (
        <main>
            {/* By adding Expression of jsx have condition used of Ternary condition which is to define if the id item list having length or value is it execute true in ternary,  while the default false if ever has no length or value it will be execute false */}
            {items.length ? (
                <ItemList
                    items={items}
                    handleCkeck={handleCkeck}
                    handleDelete={handleDelete}
                />
            ) : (
                <p style={{ marginTop: '2rem' }}>Your list is Empty</p>
            )}
        </main >

    )
}

export default Content



